package cl.falabella.evaluation;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import cl.falabella.evaluation.controller.ProductController;
import cl.falabella.evaluation.exception.NotFoundException;
import cl.falabella.evaluation.model.ProductRq;
import cl.falabella.evaluation.service.ProductServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;


import cl.falabella.evaluation.model.Product;
import cl.falabella.evaluation.repository.ProductRepository;

import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.swing.text.html.Option;
import javax.validation.ConstraintViolationException;

import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.*;

@ExtendWith(SpringExtension.class)
public class ProductServiceImplTest {

	private ProductServiceImpl productService;

	@Mock
	private ProductRepository productRepository;
	ProductRq productRq;

	@BeforeEach
	void setUp(){
		productService = new ProductServiceImpl(productRepository);
		productRq = ProductRq.builder()
				.sku("FAL-8406290")
				.name("500 Zapatilla Urbana Mujer")
				.brand("NEW BALANCE")
				.size("37")
				.price(new BigDecimal(42990.00))
				.otherImage("https://falabella.scene7.com/is/image/Falabella/8406270_1")
				.principalImage("https://falabella.scene7.com/is/image/Falabella/8406270_1").build();
	}

	@Test
    public void wheCreateAProductThenReturnNewProduct() throws Exception {

		Product newProduct = Product.builder()
				.id(1L)
				.sku(productRq.getSku())
				.name(productRq.getName())
				.size(productRq.getSize())
				.brand(productRq.getBrand())
				.principalImage(productRq.getPrincipalImage())
				.otherImage(productRq.getOtherImage()).build();

		when(productRepository.save(any())).thenReturn(newProduct);

		Assertions.assertEquals(Optional.of(1L), Optional.of(productService.create(productRq).getId()));
	}

	@Test
	public void wheCreateADuplicateProductThenReturnConstraintException() throws Exception {

		when(productRepository.save(any())).thenThrow(ConstraintViolationException.class);
		Assertions.assertThrows(Exception.class,()->{
				productService.create(productRq);
		});
	}

	@Test
	public void WhenListProductThenReturnList() throws Exception {
		Product newProduct = Product.builder()
				.id(1L)
				.sku(productRq.getSku())
				.name(productRq.getName())
				.size(productRq.getSize())
				.brand(productRq.getBrand())
				.principalImage(productRq.getPrincipalImage())
				.otherImage(productRq.getOtherImage()).build();

		List<Product> listProducts = new ArrayList<Product>();
		listProducts.add(newProduct);
		when(productRepository.findAll()).thenReturn(listProducts);

		Assertions.assertEquals(1, productService.list().size());
	}

	@Test
	public void WhenListProductIsEmptyThenReturnException() throws Exception {

		List<Product> listProducts = new ArrayList<Product>();
		when(productRepository.findAll()).thenReturn(listProducts);

		Assertions.assertThrows(NotFoundException.class,()->{
			productService.list();
		});
	}

	@Test
	public void WhenfindProductThenReturnAProduct() throws Exception {
		Product product = Product.builder()
				.id(1L)
				.sku(productRq.getSku())
				.name(productRq.getName())
				.size(productRq.getSize())
				.brand(productRq.getBrand())
				.principalImage(productRq.getPrincipalImage())
				.otherImage(productRq.getOtherImage()).build();
		when(productRepository.findBySku(anyString())).thenReturn(Optional.of(product));
		Assertions.assertEquals("37", productService.findBySKU(anyString()).getSize());
	}

	@Test
	public void WhenNotfindProductThenReturnException() throws Exception {
		when(productRepository.findBySku(anyString())).thenThrow(NotFoundException.class);
		Assertions.assertThrows(NotFoundException.class,()->{
			productService.findBySKU(anyString());
		});
	}

	@Test
	public void WhenUpdateProductThenReturnProductUpdated(){
		Product product = Product.builder()
				.id(1L)
				.sku("SKU")
				.name("Name")
				.size("32")
				.brand("ABC")
				.principalImage("http://www.google.com/test")
				.otherImage("http://www.google.com/test").build();
		when(productRepository.findBySku(anyString())).thenReturn(Optional.of(product));
		Product productUpdated = Product.builder()
				.id(1L)
				.sku(productRq.getSku())
				.name(productRq.getName())
				.size(productRq.getSize())
				.brand(productRq.getBrand())
				.principalImage(productRq.getPrincipalImage())
				.otherImage(productRq.getOtherImage()).build();
		when(productRepository.save(any())).thenReturn(productUpdated);

		Assertions.assertEquals("FAL-8406290", productService.update(productRq).getSku());
	}

	@Test
	public void WhenDeleteProductThenOperationSuccesfull(){
		Product productDelete = Product.builder()
				.id(1L)
				.sku("SKU")
				.name("Name")
				.size("32")
				.brand("ABC")
				.principalImage("http://www.google.com/test")
				.otherImage("http://www.google.com/test").build();
		when(productRepository.findBySku(anyString())).thenReturn(Optional.of(productDelete));

		Assertions.assertDoesNotThrow(() -> {
			productService.delete(anyString());
		});
	}
}
