package cl.falabella.evaluation;

import cl.falabella.evaluation.controller.ProductController;
import cl.falabella.evaluation.model.Product;
import cl.falabella.evaluation.model.ProductRq;
import cl.falabella.evaluation.service.ProductService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ProductControllerTest {

	@Mock
	private ProductService productService;

	@InjectMocks
	private ProductController productController;

	@Test
	public void WhenGetProductTheSuccessfulProcess() {

		Product product = Product.builder().id(1L).sku("FAL-10000001").build();
		when(productService.findBySKU(anyString())).thenReturn(product);
		ResponseEntity response = Assertions.assertDoesNotThrow(
				() -> productController.getProduct(anyString()));

		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());

	}

	@Test
	public void WhenCreateProductTheSuccessfulProcess() throws Exception {

		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		ProductRq productRq = ProductRq.builder()
				.sku("FAL-10000001").build();
		Product product = Product.builder().id(1L).sku("FAL-10000001").build();
		when(productService.create(productRq)).thenReturn(product);
		ResponseEntity response = Assertions.assertDoesNotThrow(
				() -> productController.createProduct(productRq, httpServletRequest));

		Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode());

	}

	@Test
	public void WhenUpdateProductTheSuccessfulProcess() throws Exception {

		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		ProductRq productRq = ProductRq.builder()
				.sku("FAL-10000001").build();
		Product product = Product.builder().id(1L).sku("FAL-10000001").build();
		when(productService.update(productRq)).thenReturn(product);
		ResponseEntity response = Assertions.assertDoesNotThrow(
				() -> productController.updateProduct(productRq, httpServletRequest));

		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void WhenDeleteProductTheSuccessfulProcess() throws Exception {
		String sku = "FAL-10000001";
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		doNothing().when(productService).delete(sku);
		ResponseEntity response = Assertions.assertDoesNotThrow(
				() -> productController.deleteProduct(sku, httpServletRequest));

		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void WhenListProductTheSuccessfulProcess() throws Exception {

		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		ProductRq productRq = ProductRq.builder()
				.sku("FAL-10000001").build();
		Product product = Product.builder().id(1L).sku("FAL-10000001").build();
		List<Product> productList = new ArrayList<Product>();
		productList.add(product);
		when(productService.list()).thenReturn(productList);
		ResponseEntity response = Assertions.assertDoesNotThrow(
				() -> productController.listProduct( httpServletRequest));

		Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());

	}

}
