package cl.falabella.evaluation.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.falabella.evaluation.model.ProductRq;
import cl.falabella.evaluation.service.ProductService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping(value = "/{sku}")
    public ResponseEntity getProduct(@PathVariable String sku) {
        return ResponseEntity.status(HttpStatus.OK).body(productService.findBySKU(sku));
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createProduct(@RequestBody @Valid ProductRq productRq,
                                        HttpServletRequest httpServletRequest) throws Exception {
        return ResponseEntity.status(HttpStatus.CREATED).body(  productService.create(productRq));
    }

    @GetMapping(value = "/list")
    public ResponseEntity listProduct(HttpServletRequest httpServletRequest) {
        return ResponseEntity.status(HttpStatus.OK).body(  productService.list());
    }

    @PatchMapping(value = "/update")
    public ResponseEntity updateProduct(@RequestBody @Valid ProductRq productRq,
                                        HttpServletRequest httpServletRequest) {
        return ResponseEntity.status(HttpStatus.OK).body(  productService.update(productRq));
    }

    @DeleteMapping(value = "/delete/{sku}")
    public ResponseEntity deleteProduct(@PathVariable("sku") String sku,
                                        HttpServletRequest httpServletRequest) {
        productService.delete(sku);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}