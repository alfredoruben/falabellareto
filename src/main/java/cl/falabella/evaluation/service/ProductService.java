package cl.falabella.evaluation.service;

import java.util.List;

import cl.falabella.evaluation.model.Product;
import cl.falabella.evaluation.model.ProductRq;

public interface ProductService {

    Product create(ProductRq productRq)throws Exception;

    List<Product> list();

    Product findBySKU(String sku);

    Product update(ProductRq productRq);

    void delete(String sku);

}
