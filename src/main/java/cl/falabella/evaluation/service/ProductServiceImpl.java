package cl.falabella.evaluation.service;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.stereotype.Service;

import cl.falabella.evaluation.exception.NotFoundException;
import cl.falabella.evaluation.model.Product;
import cl.falabella.evaluation.model.ProductRq;
import cl.falabella.evaluation.repository.ProductRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService{

    private final ProductRepository productRepository;

    public Product create(ProductRq productRq) throws Exception {
        try {
            Product newProduct = Product.builder()
                    .sku(productRq.getSku())
                    .name(productRq.getName())
                    .brand(productRq.getBrand())
                    .size(productRq.getSize())
                    .price(productRq.getPrice())
                    .otherImage(productRq.getOtherImage())
                    .principalImage(productRq.getPrincipalImage()).build();
            return productRepository.save(newProduct);
        } catch (ConstraintViolationException e) {
            throw new Exception("sku duplicado");
        }
    }

    public List<Product> list()
    {
        var products = (List<Product>) productRepository.findAll();
        if (products.isEmpty()){
            throw new NotFoundException();
        }
        return products;
    }

    public Product findBySKU(String sku) {
       return productRepository.findBySku(sku).
               orElseThrow( () -> new NotFoundException(sku));
    }

    public Product update(ProductRq productRq) {
        var updateProduct = findBySKU(productRq.getSku());
        updateProduct.setName(productRq.getName());
        updateProduct.setPrice(productRq.getPrice());
        updateProduct.setBrand(productRq.getBrand());
        updateProduct.setSize(productRq.getSize());
        updateProduct.setPrincipalImage(productRq.getPrincipalImage());
        updateProduct.setOtherImage(productRq.getOtherImage());
        return productRepository.save(updateProduct);
    }

    public void delete(String sku) {
        var deleteProduct = findBySKU(sku);
        productRepository.delete(deleteProduct);
    }
}
