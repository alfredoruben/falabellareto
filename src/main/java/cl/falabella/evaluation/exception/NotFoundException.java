package cl.falabella.evaluation.exception;

public class NotFoundException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public NotFoundException(){
        super("No se encontraron datos");
    }
    public NotFoundException(String sku){
        super(String.format("No se encontraron datos del producto %s:", sku) );
    }
}
