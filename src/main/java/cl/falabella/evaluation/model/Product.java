package cl.falabella.evaluation.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@AllArgsConstructor
@Builder
@Table(name = "products")
public class Product  implements Serializable{

    public Product(){
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(unique=true)
    private String sku;

    @NotNull
    private String name;

    @NotNull
    private String brand;

    private String size;

    @NotNull
    private BigDecimal price;

    @NotNull
    private String principalImage;

    
    private String otherImage;

}
