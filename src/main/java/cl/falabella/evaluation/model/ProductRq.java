package cl.falabella.evaluation.model;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.URL;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ProductRq {

    @NotNull
    @Pattern(regexp = "^FAL-[1-9][0-9]{6}$")
    private String sku;

    @NotNull
    @NotEmpty
    @Size(min = 3, max = 50)
    private String name;

    @NotNull
    @NotEmpty
    @Size(min = 3, max = 50)
    private String brand;

    @NotEmpty
    private String size;

    @NotNull
    @DecimalMin("1.00")
    @DecimalMax("99999999.00")
    private BigDecimal price;

    @NotNull
    @URL(regexp = "^(http|https).*" )    
    private String principalImage;

    @URL(regexp = "^(http|https).*" )
    private String otherImage;
}
