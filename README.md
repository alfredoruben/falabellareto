# Falabella Reto

Aplicacion Spring boot, JPA, H2.

## Installation

git clone https://alfredoruben@bitbucket.org/alfredoruben/falabellareto.git

```bash
cd falabellareto
```

## generar empaquetado
```bash
mvn clean install
```
# acceder directorio 'target'
```bash
cd target
```


# el JAVA_HOME tiene que estar seteado con jkd 11

```

## arrancar la aplicacion
java -jar evaluation.jar
```
# Importar la coleccion
``` 
falabella.postman_collection.jsonpostman en su aplicativo ubicado en la carpeta de src\main\resources y probar la invocaciones
```

## url swagger
[swagger](http://localhost:8080/swagger-ui.html/)
```
# reporte de covertura de codigo

directorio /target/site/jacoco/index.html
```
```
# POST
```bash
localhost:8080/products/create

request:    {
        "sku":"FAL-8406270",
        "name":"322",
        "brand":"brand 1",
        "size":"rrr",
        "price":"1.00",
        "principalImage":"https://falabella.scene7.com/is/",        
        "otherImage":"http://wewewewe.com"
       
    }

```
# GET obtener
```bash
localhost:8080/products/FAL-8406270
```
# DELETE 
```bash
localhost:8080/products/delete/FAL-8406270
```
# PATCH
```bash
localhost:8080/products/update
request
   {
        "sku":"FAL-8406270",
        "name":"322",
        "brand":"brand 5",
        "size":"rrr",
        "price":"1.00",
        "principalImage":"https://falabella.scene7.com/is/",        
        "otherImage":"http://wewewewe.com"
       
    }
```
# GET Listar
```bash
localhost:8080/products/list